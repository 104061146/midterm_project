const tags = [
    "a", "abbr", "address", "area", "article", "aside", "audio", "b", "base",
    "bdi", "bdo", "blockquote", "body", "br", "button", "canvas", "caption", "cite",
    "code", "col", "colgroup", "data", "datalist", "dd", "del", "details", "dfn", "dialog",
    "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "footer",
    "form", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hgroup", "hr",
    "html", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "legend", "li",
    "link", "main", "map", "mark", "math", "menu", "menuitem", "meta", "meter", "nav",
    "noscript",
    "object", "ol", "optgroup", "option", "output", "p", "param", "picture", "pre",
    "progress", "q", "rb", "rp", "rt", "rtc", "ruby", "s", "samp", "script",
    "section", "select", "slot", "small", "source", "span", "strong", "style", "sub",
    "summary", "sup", "svg", "table", "tbody", "td", "template", "textarea", "tfoot",
    "th", "thead", "time", "title", "tr", "track", "u", "ul", "var", "video", "wbr"
];
const full = new RegExp(tags.map(x => `<${x}\\b[^>]*>`).join('|'), 'i');
checkHTML = input => full.test(input);


String.prototype.format = function () {
    var a = this;
    for (var k in arguments) {
        a = a.replace(new RegExp("\\{" + k + "\\}", 'g'), arguments[k]);
    }
    return a
}

class Post {
    constructor(snapshot) {
        var postData = snapshot.val();

        this.email = postData['email'];
        this.post = postData['post'];
        this.title = postData['title'];
        this.profile_photo = postData['userPhoto'];
        this.theme = postData['theme'];
        this.time = postData['time'];
        this.user_name = '';
        this.key = snapshot.key;

        var replyArray = [];
        var replyStr = '';
        this.replyStr = replyStr;

        this.reply_btn_id = this.key + '-reply-btn';
        this.delete_btn_id = this.key + '-delete-btn';
        this.text_area_id = this.key + '-text-area';

        this.reply_btn = undefined;
        this.delete_btn = undefined;
        this.text_area = undefined;

        var replyRef = firebase.database().ref('com_list2/{0}/reply'.format(this.key));
        replyRef.on('child_added', function (data) {
            var newReply = new Reply(data);
            replyArray.push(newReply);
            replyStr += newReply.show_html();
        });
        this.replyStr = replyStr;
    }
    show_html(showReply = false) {
        var str_before = "<div class='my-3 p-3 bg-white rounded box-shadow smb'><h6 class='border-bottom border-gray pb-2 mb-0'></h6><div class='media text-muted pt-3'>";
        var str_before_username2 = "<p class='media-body pb-3 mb-0 lh-125 border-gray'><strong class='d-block text-gray-dark'>";
        if (this.profile_photo == '' || this.profile_photo == undefined) {
            var profile_photo = "<img src='img/bg.png' alt='' class='mr-2 rounded' style='height:24px; width:24px;'></img>";
        } else {
            var profile_photo = "<img src={0} alt='' class='mr-2 rounded' style='height:24px; width:24px;'></img>".format(this.profile_photo);
        }
        var buttons = '<button type="button" class="btn btn-light btn-sm" id="{0}-read-btn" data-toggle="modal" data-target="#exampleModalScrollable">read more ...</button>'.format(this.key);

        var time = "<p class=\"font-weight-light text-muted text-sm badge badge-light\">{0}</p>".format(this.time);
        var titleHTML = "<h5>{0}</h5>".format(this.title);

        var post_content = this.post;
        if (this.post.length > 50) {
            post_content = post_content.substring(0, 200) + '...'
        }
        var close_btn = '<button type="button" class="close" aria-label="Close" id="{0}"><span aria-hidden="true">&times;</span></button>'.format(this.delete_btn_id)
        var postHTML = "<p class='d-block text-gray-dark pb-3 mb-0 lh-125'>" + post_content + "</p>" + buttons;
        var str_after_content =  "</div>" + titleHTML + postHTML + "</div>";

        return str_before + profile_photo + str_before_username2 + this.email + "<br>" +time + "</strong>" + close_btn + "</p>" + str_after_content;
    }

    singleHTML() {
        var replys = '';
        var str_before = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'>";
        var str_before_username2 = "<p class='media-body pb-3 mb-0 lh-125 border-gray'><strong class='d-block text-gray-dark'>";
        if (this.profile_photo == '' || this.profile_photo == undefined) {
            var profile_photo = "<img src='img/bg.png' alt='' class='mr-2 rounded' style='height:24px; width:24px;'></img>";
        } else {
            var profile_photo = "<img src={0} alt='' class='mr-2 rounded' style='height:24px; width:24px;'></img>".format(this.profile_photo);
        }

        var postHTML = "<p class='d-block text-gray-dark pb-3 mb-0 lh-125'>" + this.post.replace('\n', '<br>') + "</p>";
        var str_after_content = "</div>" + postHTML + "</div>";

        return str_before + profile_photo + str_before_username2 + this.email + "</strong>" + "</p>" + str_after_content;
    }

    update() {
        document.getElementById(this.key + '-read-btn').addEventListener('click', postDisplay);
        document.getElementById(this.delete_btn_id).addEventListener('click', function (event) {
            var id = this.id.replace('-delete-btn', '');
            var postRef = firebase.database().ref('com_list2/{0}'.format(id));
            postRef.remove();
        });
    };

    reply_callback(event) {
        var text_id = this.id.replace('-reply-btn', '-text-area');
        var post_id = this.id.replace('-reply-btn', '');
        var text_area = document.getElementById(text_id);
        var reply_text = text_area.value;
        // var reply_text = 'test reply';
        text_area.value = '';
        var replyRef = firebase.database().ref('com_list2/{0}/reply'.format(post_id)).push();
        replyRef.set({
                email: user_email,
                post: reply_text,
                userPhoto: user_photo
            }).then(function () {
                console.log('Synchronization succeeded');
            })
            .catch(function () {
                console.log('Synchronization failed');
            });
    };
}

class Reply {
    constructor(snapshot) {
        var postData = snapshot.val();
        this.email = postData['email'];
        this.post = postData['post'];
        this.profile_photo = postData['userPhoto'];
        this.user_name = '';
        this.key = snapshot.key;
    }
    show_html() {
        var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow '><div class='media text-muted pt-3 border-bottom border-gray'>";
        var str_before_username2 = "<p class='media-body pb-3 mb-0 small lh-125 '><strong class='d-block text-gray-dark'>";
        if (this.profile_photo == '' || this.profile_photo == undefined) {
            var profile_photo = "<img src='img/bg.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'></img>";
        } else {
            var profile_photo = "<img src={0} alt='' class='mr-2 rounded' style='height:32px; width:32px;'></img>".format(this.profile_photo);
        }
        var str_after_content = "</p></div></div>";
        return str_before_username + profile_photo + str_before_username2 + this.email + "</strong>" + this.post.replace('\n', '<br>') + str_after_content;
    }
}

var user_email = '';
var user_photo = '';
var current_id = '';
var globalPostArray = [];
var firstLoad = true;
var new_btn = document.getElementById('new-submit-btn');
var comment_content = document.getElementById('comment-content');
var comment_btn = document.getElementById('comment-btn');
var new_post_title = document.getElementById('new-post-title');
var new_post_content = document.getElementById('new-post-content');
var sortType = 0;


new_btn.addEventListener('click', function (event) {
    if (new_post_content.value != "") {
        var newPostRef = firebase.database().ref('com_list2').push();

        if (checkHTML(new_post_title.value)) {
            alert('Title can\'t contain HTML code');
            return
        }
        if (checkHTML(new_post_content.value)) {
            alert('Post can\'t contain HTML code');
            return
        }


        newPostRef.set({
                title: new_post_title.value,
                email: user_email,
                post: new_post_content.value,
                userPhoto: user_photo,
                time: parseTime()
            }).then(function () {
                console.log('comment succeeded');
                $('#new-post-modal').modal('hide');
            })
            .catch(function () {
                console.log('comment failed');
            });

        new_post_content.value = "";
        new_post_title.value = "";
    }
});

comment_btn.addEventListener('click', leaveComment);

function postDisplay(event) {
    current_id = this.id.replace('-read-btn', '');
    updateDisplay();
}


function updateDisplay() {
    var ref = firebase.database().ref('com_list2/{0}'.format(current_id));

    ref.once('value')
        .then(function (snapshot) {
            document.getElementById("post-content").innerHTML = singlePostHTML(snapshot);
            globalPostArray.forEach(p => p.update());
        })
        .catch(e => console.log('post error:', e.message));
}

function leaveComment(event) {
    if (comment_content.value != "") {
        var newPostRef = firebase.database().ref('com_list2/{0}/reply'.format(current_id)).push();

        if (checkHTML(comment_content.value)) {
            alert('Comment can\t contain HTML code');
            return
        }

        newPostRef.set({
                email: user_email,
                post: comment_content.value,
                userPhoto: user_photo,
                time: parseTime()
            }).then(function () {
                console.log('comment succeeded');
            })
            .catch(function () {
                console.log('comment failed');
            });

        comment_content.value = "";
        updateDisplay();
    }
}


function singlePostHTML(snapshot) {
    var str_after_content = "</p></div>";
    var postObj = new Post(snapshot);
    var replyArray = [];
    var replyRef = snapshot.child('reply');

    document.getElementById("post-title").innerHTML = postObj.title;
    console.log(postObj.title);
    replyRef.forEach(function (subChild) {
        var replyObj = new Reply(subChild);
        replyArray.push(replyObj.show_html());
    });

    return postObj.singleHTML() + replyArray.join('') + str_after_content;
}

function postHTML(snapshot) {
    var str_after_content = "</p></div>";
    globalPostArray = [];
    var postArray = [];

    snapshot.forEach(function (childSnapshot) {
        var postObj = new Post(childSnapshot);
        globalPostArray.push(postObj);
        // postArray.push(postObj.show_html() + str_after_content);
    });
    switch (sortType){
        case 0: globalPostArray.sort(sortTimeFn);
        break;
        case 1: globalPostArray.sort(sortEmailFn);
        break;
        case 2: globalPostArray.sort(sortTitleFn);
        break;
    }

    globalPostArray.forEach(function (x){
        postArray.push(x.show_html() + str_after_content);
    });
    return postArray.join('');
}

function parseTime() {
    var date = new Date();
    var mon = Number(date.getMonth())+1;
    return date.getFullYear() + '.' + mon + '.' + date.getDate();
}

function init() {

    firebase.auth().onAuthStateChanged(function (user) {
        console.log('user:', user);
        var menu = document.getElementById('navbarDropdown');
        if (user) {
            user_email = user.email;
            menu.innerHTML = user.email;
            user_photo = user.photoURL;

            var udata = null;
            var emailRef = firebase.database().ref('user/' + user.email.replace(/\./g, ''));
            emailRef.once('value', function (u) {
                udata = u.val();
            });

            var userRef = firebase.database().ref('user/' + user.uid);
            userRef.once('value', function (snapshot) {

                if (snapshot.val() != null) {
                    console.log('user_ref exist');
                } else if (udata != null) {
                    userRef.set({
                        email: user.email,
                        name: udata['name'],
                        photo: user.photoURL,
                        birthday: '',
                        gender: '',
                        bio: '',
                        phone: ''
                    });
                    console.log('create user_ref');
                } else {
                    userRef.set({
                        email: user.email,
                        name: user.displayName,
                        photo: user.photoURL,
                        birthday: '',
                        gender: '',
                        bio: '',
                        phone: ''
                    });
                    console.log('create user_ref');
                }

            });

            var logout = document.getElementById('logout-btn');
            logout.addEventListener('click', function () {
                firebase.auth().signOut().then(function () {
                    console.log('signout successfully');
                }).catch(function (error) {
                    console.log(error);
                });
                document.location.href = 'index.html';
            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
            user_email = '';
            user_photo = '';
        }
    });



    var postsRef = firebase.database().ref('com_list2');
    var total_post = []
    var first_count = 0;
    var second_count = 0;

    postsRef.on('child_added', function (snapshot) {
        if (!firstLoad)
            notifyNewPost();
    });

    postsRef.on('value', function (snapshot) {
        document.getElementById("post_list").innerHTML = postHTML(snapshot);
        globalPostArray.forEach(p => p.update());
        firstLoad = false;
    });
}

window.onload = function () {
    init();
};

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

function notifyNewPost() {
    if (!("Notification" in window)) {
        alert("This browser does not support desktop notification");
    } else if (Notification.permission === "granted") {
        var notification = new Notification("NTHU Forum", {
            body: "New Post Available!"
        });
    } else if (Notification.permission !== "denied") {
        Notification.requestPermission().then(function (permission) {
            if (permission === "granted") {
                var notification = new Notification("NTHU Forum", {
                    body: "New Post Available!"
                });
            }
        });
    }
}

function sortTimeFn(a, b){
    if (a.time > b.time){
        return -1;
    }
    else if (a.time < b.time){
        return 1;
    }
    else{
        return 0;
    }
}
function sortTitleFn(a, b){
    if (a.title > b.title){
        return 1;
    }
    else if (a.title < b.title){
        return -1;
    }
    else{
        return 0;
    }
}
function sortEmailFn(a, b){
    if (a.email > b.email){
        return 1;
    }
    else if (a.email < b.email){
        return -1;
    }
    else{
        return 0;
    }
}

document.getElementById('sort-time').addEventListener('click', function (event) {  
    console.log('time');
    sortType = 0;

    firebase.database().ref('com_list2').once('value').then(function (snapshot) {
        document.getElementById("post_list").innerHTML = postHTML(snapshot);
        globalPostArray.forEach(p => p.update());
        firstLoad = false;
    });
});
document.getElementById('sort-email').addEventListener('click', function (event) {  
    console.log('email');
    sortType = 1;
    firebase.database().ref('com_list2').once('value').then(function (snapshot) {
        document.getElementById("post_list").innerHTML = postHTML(snapshot);
        globalPostArray.forEach(p => p.update());
        firstLoad = false;
    });
});
document.getElementById('sort-title').addEventListener('click', function (event) {  
    console.log('title');
    sortType = 2;
    firebase.database().ref('com_list2').once('value').then(function (snapshot) {
        document.getElementById("post_list").innerHTML = postHTML(snapshot);
        globalPostArray.forEach(p => p.update());
        firstLoad = false;
    });
});