var menu = document.getElementById('navbarDropdown');
var user_name = document.getElementById('user_name');
var user_email = document.getElementById('user_email');
var user_image = document.getElementById('user_image');
var signin_time = document.getElementById('signin-time');

var user_email_str = '';
var user_photo_str = '';
var current_id = '';
var globalPostArray = [];
var uid = 0;

var new_btn = document.getElementById('new-submit-btn');
var comment_content = document.getElementById('comment-content');
var comment_btn = document.getElementById('comment-btn');
var new_post_title = document.getElementById('new-post-title');
var new_post_content = document.getElementById('new-post-content');

var newWorkSubmitBtn = document.getElementById('new-work-submit-btn');
var inputCompany = document.getElementById('input-company');
var inputPosition = document.getElementById('input-position');
var inputWorkFrom = document.getElementById('input-work-from');
var inputWorkTo = document.getElementById('input-work-to');

var newEduSubmitBtn = document.getElementById('new-edu-submit-btn');
var inputSchool = document.getElementById('input-school');
var eduType = document.getElementById('edu-type');
var inputMajor = document.getElementById('input-major');
var inputEduFrom = document.getElementById('input-edu-from');
var inputEduTo = document.getElementById('input-edu-to');

var newInfoSubmitBtn = document.getElementById('new-info-submit-btn');
var inputEmail = document.getElementById('input-email');
var inputPhone = document.getElementById('input-phone');
var inputAddress = document.getElementById('input-address');
var inputBirthday = document.getElementById('input-birthday');
var inputGender = document.getElementById('gender-type');
var inputBoold = document.getElementById('boold-type');

var infoEmail = document.getElementById('info-email');
var infoPhone = document.getElementById('info-phone');
var infoAddress = document.getElementById('info-address');
var infoBirthday = document.getElementById('info-birthday');
var infoGender = document.getElementById('info-gender');
var infoBoold = document.getElementById('info-boold');




const tags = [
    "a", "abbr", "address", "area", "article", "aside", "audio", "b", "base",
    "bdi", "bdo", "blockquote", "body", "br", "button", "canvas", "caption", "cite",
    "code", "col", "colgroup", "data", "datalist", "dd", "del", "details", "dfn", "dialog",
    "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "footer",
    "form", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hgroup", "hr",
    "html", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "legend", "li",
    "link", "main", "map", "mark", "math", "menu", "menuitem", "meta", "meter", "nav",
    "noscript",
    "object", "ol", "optgroup", "option", "output", "p", "param", "picture", "pre",
    "progress", "q", "rb", "rp", "rt", "rtc", "ruby", "s", "samp", "script",
    "section", "select", "slot", "small", "source", "span", "strong", "style", "sub",
    "summary", "sup", "svg", "table", "tbody", "td", "template", "textarea", "tfoot",
    "th", "thead", "time", "title", "tr", "track", "u", "ul", "var", "video", "wbr"
];
const full = new RegExp(tags.map(x => `<${x}\\b[^>]*>`).join('|'), 'i');
checkHTML = input => full.test(input);


String.prototype.format = function () {
    var a = this;
    for (var k in arguments) {
        a = a.replace(new RegExp("\\{" + k + "\\}", 'g'), arguments[k]);
    }
    return a
}

firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        uid = user.uid;
        establishListener();
        menu.innerHTML = user.email;
        user_email_str = user.email;
        user_email.innerHTML = 'E-mail: ' + user.email;
        if (user.photoURL)
            user_image.src = user.photoURL;
        else
            user_image.src = 'img/bg.png';
        // user_name.innerHTML = user.displayName;
        var logout = document.getElementById('logout-btn');
        logout.addEventListener('click', function () {
            firebase.auth().signOut().then(function () {
                document.getElementById('page-self').innerHTML = "";
                console.log('signout successfully');
            }).catch(function (error) {
                console.log(error);
            });
            document.location.href = 'index.html';
        });
    } else {
        menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
    }
});


class Post {
    constructor(snapshot) {
        var postData = snapshot.val();

        this.email = postData['email'];
        this.post = postData['post'];
        this.title = postData['title'];
        this.profile_photo = postData['userPhoto'];
        this.theme = postData['theme'];
        this.time = postData['time'];
        this.user_name = '';
        this.key = snapshot.key;

        var replyArray = [];
        var replyStr = '';
        this.replyStr = replyStr;

        this.reply_btn_id = this.key + '-reply-btn';
        this.delete_btn_id = this.key + '-delete-btn';
        this.text_area_id = this.key + '-text-area';

        this.reply_btn = undefined;
        this.delete_btn = undefined;
        this.text_area = undefined;

        var replyRef = firebase.database().ref('com_list/{0}/reply'.format(this.key));
        replyRef.on('child_added', function (data) {
            var newReply = new Reply(data);
            replyArray.push(newReply);
            replyStr += newReply.show_html();
        });
        this.replyStr = replyStr;
    }
    show_html(showReply = false) {
        var str_before = "<div class='my-3 p-3 bg-white rounded box-shadow smb border-bottom border-gray'><div class='media text-muted pt-3'>";
        var str_before_username2 = "<p class='media-body pb-3 mb-0 lh-125 border-gray'><strong class='d-block text-gray-dark'>";
        if (this.profile_photo == '' || this.profile_photo == undefined) {
            var profile_photo = "<img src='img/bg.png' alt='' class='mr-2 rounded' style='height:24px; width:24px;'></img>";
        } else {
            var profile_photo = "<img src={0} alt='' class='mr-2 rounded' style='height:24px; width:24px;'></img>".format(this.profile_photo);
        }
        var buttons = '<button type="button" class="btn btn-light btn-sm" id="{0}-read-btn" data-toggle="modal" data-target="#exampleModalScrollable">read more ...</button>'.format(this.key);

        var titleHTML = "<h5>{0}</h5>".format(this.title);

        var post_content = this.post;
        if (this.post.length > 50) {
            post_content = post_content.substring(0, 200) + '...'
        }

        var postHTML = "<p class=\"d-block text-gray-dark text-gray pb-3 mb-0 lh-125\">" + post_content + "</p>" + buttons;

        var str_after_content = "</div>" + titleHTML + postHTML + "</div>";

        return str_before + profile_photo + str_before_username2 + this.email + "</strong>" + "</p>" + str_after_content;
    }

    singleHTML() {
        var replys = '';
        var str_before = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'>";
        var str_before_username2 = "<p class='media-body pb-3 mb-0 lh-125 border-gray'><strong class='d-block text-gray-dark'>";
        if (this.profile_photo == '' || this.profile_photo == undefined) {
            var profile_photo = "<img src='img/bg.png' alt='' class='mr-2 rounded' style='height:24px; width:24px;'></img>";
        } else {
            var profile_photo = "<img src={0} alt='' class='mr-2 rounded' style='height:24px; width:24px;'></img>".format(this.profile_photo);
        }
        var time = "<p class=\"font-weight-light text-muted text-sm badge badge-light\">{0}</p>".format(this.time)
        
        var postHTML = "<p class='d-block text-gray-dark text-gray pb-3 mb-0 lh-125 smtext'>" + this.post + "</p>";
        var str_after_content = "</div>" + postHTML + "</div>";

        return str_before + profile_photo + str_before_username2 + this.email + time+"</strong>" + "</p>" + str_after_content;
    }

    update() {
        document.getElementById(this.key + '-read-btn').addEventListener('click', postDisplay);
    };

    reply_callback(event) {



        var text_id = this.id.replace('-reply-btn', '-text-area');
        var post_id = this.id.replace('-reply-btn', '');
        var text_area = document.getElementById(text_id);
        var reply_text = text_area.value;
        // var reply_text = 'test reply';
        if (checkHTML(reply_text)) {
            alert('Comment can\'t contain HTML code');
            return
        }

        text_area.value = '';
        var replyRef = firebase.database().ref('com_list/{0}/reply'.format(post_id)).push();
        replyRef.set({
                email: user_email,
                post: reply_text,
                userPhoto: user_email_str
            }).then(function () {
                console.log('Synchronization succeeded');
            })
            .catch(function () {
                console.log('Synchronization failed');
            });
    };

    delete_callback(event) {

    };
}

class Reply {
    constructor(snapshot) {
        var postData = snapshot.val();
        this.email = postData['email'];
        this.post = postData['post'];
        this.profile_photo = postData['userPhoto'];
        this.user_name = '';
        this.key = snapshot.key;
    }
    show_html() {
        var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow '><div class='media text-muted pt-3 border-bottom border-gray'>";
        var str_before_username2 = "<p class='media-body pb-3 mb-0 small lh-125 '><strong class='d-block text-gray-dark'>";
        if (this.profile_photo == '' || this.profile_photo == undefined) {
            var profile_photo = "<img src='img/bg.png' alt='' class='mr-2 rounded' style='height:32px; width:32px;'></img>";
        } else {
            var profile_photo = "<img src={0} alt='' class='mr-2 rounded' style='height:32px; width:32px;'></img>".format(this.profile_photo);
        }
        var str_after_content = "</p></div></div>";
        return str_before_username + profile_photo + str_before_username2 + this.email + "</strong>" + this.post + str_after_content;
    }
}


comment_btn.addEventListener('click', leaveComment);

function postDisplay(event) {
    current_id = this.id.replace('-read-btn', '');
    updateDisplay();
}


function updateDisplay() {
    var ref = firebase.database().ref('com_list/{0}'.format(current_id));

    ref.once('value')
        .then(function (snapshot) {
            document.getElementById("post-content").innerHTML = singlePostHTML(snapshot);
            globalPostArray.forEach(p => p.update());
        })
        .catch(e => console.log('post error:', e.message));
}

function leaveComment(event) {
    if (comment_content.value != "") {


        if (checkHTML(comment_content.value)) {
            alert('Comment can\'t contain HTML code');
            return
        }
        var newPostRef = firebase.database().ref('com_list/{0}/reply'.format(current_id)).push();
        newPostRef.set({
                email: user_email,
                post: comment_content.value,
                userPhoto: user_photo_str,
                time: parseTime()
            }).then(function () {
                console.log('comment succeeded');
            })
            .catch(function () {
                console.log('comment failed');
            });

        comment_content.value = "";
        updateDisplay();
    }
}


function singlePostHTML(snapshot) {
    var str_after_content = "</p></div>";
    var postObj = new Post(snapshot);
    var replyArray = [];
    var replyRef = snapshot.child('reply');

    document.getElementById("post-title").innerHTML = postObj.title;
    replyRef.forEach(function (subChild) {
        var replyObj = new Reply(subChild);
        replyArray.push(replyObj.show_html());
    });

    return postObj.singleHTML() + replyArray.join('') + str_after_content;
}

function postHTML(snapshot) {
    var str_after_content = "</p></div>";
    globalPostArray = [];
    var postArray = [];

    snapshot.forEach(function (childSnapshot) {
        var postObj = new Post(childSnapshot);
        if (childSnapshot.val()['email'] == user_email_str) {
            globalPostArray.push(postObj);
            postArray.push(postObj.show_html() + str_after_content);
        }
    });
    return postArray.join('');
}


firebase.database().ref('com_list').on('value', function (snapshot) {
    document.getElementById("post_list").innerHTML = postHTML(snapshot);
    globalPostArray.forEach(p => p.update());
});
firebase.database().ref('com_list2').on('value', function (snapshot) {
    document.getElementById("post_list2").innerHTML = postHTML(snapshot);
    globalPostArray.forEach(p => p.update());
});

newWorkSubmitBtn.addEventListener('click', function (event) {
    if (checkHTML(inputCompany.value)) {
        alert('Company name can\'t contain HTML code');
        return
    }
    if (checkHTML(inputPosition.value)) {
        alert('Position name can\'t contain HTML code');
        return
    }


    var ref = firebase.database().ref('user/{0}/work'.format(uid)).push();
    ref.set({
            company: inputCompany.value,
            position: inputPosition.value,
            from: inputWorkFrom.value,
            to: inputWorkTo.value
        }).then(function () {
            $('#new-work-modal').modal('hide');
        })
        .catch(function () {});

});

newEduSubmitBtn.addEventListener('click', function (event) {
    if (checkHTML(inputSchool.value)) {
        alert('School name can\'t contain HTML code');
        return
    }
    if (checkHTML(inputMajor.value)) {
        alert('Major can\'t contain HTML code');
        return
    }

    var ref = firebase.database().ref('user/{0}/education'.format(uid)).push();
    ref.set({
            school: inputSchool.value,
            type: eduType.value,
            major: inputMajor.value,
            from: inputEduFrom.value,
            to: inputEduTo.value
        }).then(function () {
            $('#new-education-modal').modal('hide');
        })
        .catch(function () {});

});

newInfoSubmitBtn.addEventListener('click', function(){
    if (checkHTML(inputBirthday.value)) {
        alert('Birthday can\'t contain HTML code');
        return
    }
    if (checkHTML(inputGender.value)) {
        alert('Gender can\'t contain HTML code');
        return
    }
    if (checkHTML(inputPhone.value)) {
        alert('Phone can\'t contain HTML code');
        return
    }
    if (checkHTML(inputGender.value)) {
        alert('Gender can\'t contain HTML code');
        return
    }
    if (checkHTML(inputAddress.value.value)) {
        alert('Address can\'t contain HTML code');
        return
    }

    var ref = firebase.database().ref('user/'+uid);
    console.log({
        birthday: inputBirthday.value,
        gender: inputGender.value,
        phone: inputPhone.value,
        boold: inputBoold.value,
        address: inputAddress.value
    });
    ref.set({
        birthday: inputBirthday.value,
        gender: inputGender.value,
        phone: inputPhone.value,
        boold: inputBoold.value,
        address: inputAddress.value
    }).then(()=>$('#new-info-modal').modal('hide'))
    .catch(function (err){
        console.log(err);
    });
});

function establishListener() {
    firebase.database().ref('user/' + uid).once('value', function(snapshot){
        user_name.innerHTML = snapshot.val()['name'];
    });



    var workRef = firebase.database().ref('user/' + uid + '/work');
    workRef.on('value', function (snapshot) {
        var workArray = [];
        snapshot.forEach(function (childSnapshot) {
            var data = childSnapshot.val();
            workArray.push('<h6>{0}</h6><p class="text-muted">{1}, {2} ~ {3}</p><hr>'.format(data['company'], data['position'], data['from'], data['to']))
        });
        document.getElementById('work-experience-list').innerHTML = workArray.join('');
    });

    var eduRef = firebase.database().ref('user/{0}/education'.format(uid.toString()));
    eduRef.on('value', function (snapshot) {
        var workArray = [];
        snapshot.forEach(function (childSnapshot) {
            var data = childSnapshot.val();
            workArray.push('<h6>{0}</h6><p class="text-muted">{1}, {4}, {2} ~ {3}</p><hr>'.format(data['school'], data['major'], data['from'], data['to'], data['type']));
        });
        document.getElementById('edu-experience-list').innerHTML = workArray.join('');
    });


    var infoRef = firebase.database().ref('user/{0}'.format(uid.toString()));
    infoRef.on('value', function (snapshot) {
        var infoData = snapshot.val();
        infoEmail.innerHTML = user_email_str;
        infoAddress.innerHTML = isUndefined(infoData['address']);
        infoBoold.innerHTML = isUndefined(infoData['boold']);
        infoGender.innerHTML = isUndefined(infoData['gender']);
        infoPhone.innerHTML = isUndefined(infoData['phone']);
        infoBirthday.innerHTML = isUndefined(infoData['birthday']);

        inputEmail.value = user_email_str;
        inputPhone.value = isUndefined(infoData['phone']);
        inputAddress.value = isUndefined(infoData['address']);
        inputBirthday.value = isUndefined(infoData['birthday']);
        inputGender.value = isUndefined(infoData['gender']);
        inputBoold.value = isUndefined(infoData['boold']);
    });
}

function isUndefined(x){
    if (x == undefined)
        return '';
    else
        return x;

}

function parseTime() {
    var date = new Date();
    var mon = Number(date.getMonth())+1;
    return date.getFullYear() + '.' + mon + '.' + date.getDate();
}