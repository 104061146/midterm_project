const tags = [
    "a", "abbr", "address", "area", "article", "aside", "audio", "b", "base",
    "bdi", "bdo", "blockquote", "body", "br", "button", "canvas", "caption", "cite",
    "code", "col", "colgroup", "data", "datalist", "dd", "del", "details", "dfn", "dialog",
    "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "footer",
    "form", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hgroup", "hr",
    "html", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "legend", "li",
    "link", "main", "map", "mark", "math", "menu", "menuitem", "meta", "meter", "nav",
    "noscript",
    "object", "ol", "optgroup", "option", "output", "p", "param", "picture", "pre",
    "progress", "q", "rb", "rp", "rt", "rtc", "ruby", "s", "samp", "script",
    "section", "select", "slot", "small", "source", "span", "strong", "style", "sub",
    "summary", "sup", "svg", "table", "tbody", "td", "template", "textarea", "tfoot",
    "th", "thead", "time", "title", "tr", "track", "u", "ul", "var", "video", "wbr"
];
const full = new RegExp(tags.map(x => `<${x}\\b[^>]*>`).join('|'), 'i');
checkHTML = input => full.test(input);


function initApp() {
    // Login with Email/Password
    var txtInputEmail = document.getElementById('inputEmail');
    var txtInpurPwd = document.getElementById('inputPassword');
    var inputEmail = document.getElementById('input-email');
    var inputPassword = document.getElementById('input-password');
    var inputFullname = document.getElementById('input-fullname');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnFacebook = document.getElementById('btnfacebook');
    var btnSignUp = document.getElementById('btnSignUp');

    btnLogin.addEventListener('click', function () {
        if (checkHTML(txtInputEmail.value)) {
            alert('Email can\'t contain HTML code');
            return
        }
        if (checkHTML(txtInpurPwd.value)) {
            alert('Password can\'t contain HTML code');
            return
        }


        firebase.auth().signInWithEmailAndPassword(txtInputEmail.value, txtInpurPwd.value
        ).then(function () {
            create_alert('success', 'sign successfully');
            console.log('signin');

            document.location.href = 'postlist.html';
        }).catch(function (error) {
            create_alert('error', error.message);
        });
    });

    btnGoogle.addEventListener('click', function () {

        console.log('try to use google');
        var provider = new firebase.auth.GoogleAuthProvider();

        firebase.auth().signInWithPopup(provider).then(function (result) {
            create_alert('success', 'login successfully');
            document.location.href = 'postlist.html';
        }).catch(function (error) {
            create_alert('error', error.message);
        });

    });

    btnFacebook.addEventListener('click', function () {
        console.log('try to use Facebook');
        var provider = new firebase.auth.FacebookAuthProvider();

        firebase.auth().signInWithPopup(provider).then(function (result) {
            create_alert('success', 'login successfully');
            document.location.href = 'postlist.html';
        }).catch(function (error) {
            create_alert('error', error.message);
        });
    });

    btnSignUp.addEventListener('click', function () {
        if (checkHTML(inputEmail.value)) {
            alert('Email can\'t contain HTML code');
            return
        }
        if (checkHTML(inputPassword.value)) {
            alert('Password can\'t contain HTML code');
            return
        }

        console.log('create account', inputEmail.value, inputPassword.value);
        firebase.auth().createUserWithEmailAndPassword(inputEmail.value, inputPassword.value
        ).then(function () {
            firebase.database().ref('user/'+inputEmail.value.replace(/\./g, '')).set({
                name: inputFullname.value
            });
            create_alert('success', ('create account ' + inputEmail.value));
            $('#signup-modal').modal('hide');
        }).catch(function (error) {
            alert('error: ' + error.message);
        });
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};