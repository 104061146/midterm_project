String.prototype.format = function () {
    var a = this;
    for (var k in arguments) {
        a = a.replace(new RegExp("\\{" + k + "\\}", 'g'), arguments[k]);
    }
    return a
}

class Post {
    constructor(snapshot) {
        var postData = snapshot.val();
        console.log('snapshot', snapshot);
        this.email = postData['email'];
        this.post = postData['post'];
        this.profile_photo = postData['userPhoto'];
        this.user_name = '';
        this.key = snapshot.key;
        // this.replyArray = new Array();
        var replyArray = [];
        var replyStr = '';
        this.replyStr = replyStr;

        this.reply_btn_id = this.key + '-reply-btn';
        this.delete_btn_id = this.key + '-delete-btn';
        this.text_area_id = this.key + '-text-area';

        this.reply_btn = undefined;
        this.delete_btn = undefined;
        this.text_area = undefined;

        var replyRef = firebase.database().ref('com_list/{0}/reply'.format(this.key));
        replyRef.on('child_added', function (data) {
            console.log(data);
            // console.log('child_add on');
            var newReply = new Reply(data);
            replyArray.push(newReply);
            replyStr += newReply.show_html();
            // console.log(replyStr);
            // console.log('replyArray', replyArray);
        });
        this.replyStr = replyStr;
    }
    show_html(showReply = false) {
        var replys = '';
        var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'>";
        var str_before_username2 = "<p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
        if (this.profile_photo == '') {
            var profile_photo = "<img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'></img>";
        } else {
            var profile_photo = "<img src={0} alt='' class='mr-2 rounded' style='height:32px; width:32px;'></img>".format(this.profile_photo);
        }
        var buttons = "<div class=\"btn-group-sm\" role=\"group\" aria-label=\"Basic example\"><button type=\"button\" class=\"btn btn-light opt-btn\" id=\"{0}\">Replay</button><button type=\"button\" class=\"btn btn-light opt-btn\" id=\"{1}\">Delete</button></div>".format(this.reply_btn_id, this.delete_btn_id);

        var text = "<textarea class=\"form-control\" id=\"{0}\" rows=\"1\"></textarea></div>\n".format(this.text_area_id);

        // if (showReply){
        //     this.replyArray.forEach(function(item, index, array){
        //         // console.log(item);
        //         replys += item.show_html;
        //     });

        // }
        console.log('this.replyStr', this.replyStr);

        var str_after_content = "</p></div>" + buttons + this.replyStr + text;

        return str_before_username + profile_photo + str_before_username2 + this.email + "</strong>" + this.post + str_after_content;
    }
    update() {
        this.text_area = document.getElementById(this.text_area_id);
        this.reply_btn = document.getElementById(this.reply_btn_id);
        this.delete_btn = document.getElementById(this.delete_btn_id);


        this.reply_btn.addEventListener('click', this.reply_callback);
        this.delete_btn.addEventListener('click', this.delete_callback);
    };

    reply_callback(event) {
        var text_id = this.id.replace('-reply-btn', '-text-area');
        var post_id = this.id.replace('-reply-btn', '');
        var text_area = document.getElementById(text_id);
        var reply_text = text_area.value;
        // var reply_text = 'test reply';
        text_area.value = '';
        var replyRef = firebase.database().ref('com_list/{0}/reply'.format(post_id)).push();
        replyRef.set({
                email: user_email,
                post: reply_text,
                userPhoto: user_photo
            }).then(function () {
                console.log('Synchronization succeeded');
            })
            .catch(function () {
                console.log('Synchronization failed');
            });
    };

    delete_callback(event) {

    };
}

class Reply {
    constructor(snapshot) {
        var postData = snapshot.val();
        this.email = postData['email'];
        this.post = postData['post'];
        this.profile_photo = postData['userPhoto'];
        this.user_name = '';
        this.key = snapshot.key;
    }
    show_html() {
        var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><div class='media text-muted pt-3'>";
        var str_before_username2 = "<p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
        if (this.profile_photo == '') {
            var profile_photo = "<img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'></img>";
        } else {
            var profile_photo = "<img src={0} alt='' class='mr-2 rounded' style='height:32px; width:32px;'></img>".format(this.profile_photo);
        }
        var str_after_content = "</p></div></div>";
        // console.log('show re');
        return str_before_username + profile_photo + str_before_username2 + this.email + "</strong>" + this.post + str_after_content;
    }
}

var user_email = '';
var user_photo = '';
var globalPostArray = [];

function postHTML(snapshot) {
    var str_after_content = "</p></div>";
    globalPostArray = [];
    var postArray = [];

    snapshot.forEach(function (childSnapshot) {
        var postObj = new Post(childSnapshot);
        globalPostArray.push(postObj);
        var replyArray = [];
        var replyRef = childSnapshot.child('reply');
        replyRef.forEach(function (subChild) {
            var replyObj = new Reply(subChild);
            replyArray.push(replyObj.show_html());
        });
        postArray.push(postObj.show_html() + replyArray.join('') + str_after_content);
    });
    return postArray.join('');
}

function init() {

    firebase.auth().onAuthStateChanged(function (user) {
        var menu = document.getElementById('navbarDropdown');
        if (user) {
            user_email = user.email;
            menu.innerHTML = user.email;
            user_photo = user.photoURL;

            var logout = document.getElementById('logout-btn');
            logout.addEventListener('click', function () {
                firebase.auth().signOut().then(function () {
                    console.log('signout successfully');
                }).catch(function (error) {
                    console.log(error);
                });

            });
        } else {
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
            user_email = '';
            user_photo = '';
        }
    });

    post_btn = document.getElementById('post_btn');
    post_txt = document.getElementById('comment');


    post_btn.addEventListener('click', function () {
        if (post_txt.value != "") {
            var newPostRef = firebase.database().ref('com_list').push();
            newPostRef.set({
                    email: user_email,
                    post: post_txt.value,
                    userPhoto: user_photo,
                    category: ''
                }).then(function () {
                    console.log('Synchronization succeeded');
                })
                .catch(function () {
                    console.log('Synchronization failed');
                });

            post_txt.value = "";
        }
    });


    var postsRef = firebase.database().ref('com_list');
    var total_post = []
    var first_count = 0;
    var second_count = 0;


    // postsRef.once('value')
    //     .then(function (snapshot) {
    //         document.getElementById("post_list").innerHTML = postHTML(snapshot);
    //         globalPostArray.forEach(p => p.update());
    //     })
    //     .catch(e => console.log('post error:', e.message));

    postsRef.on('value', function (snapshot) {
        document.getElementById("post_list").innerHTML = postHTML(snapshot);
        globalPostArray.forEach(p => p.update());
    });
}

function create_post(email, post, photo) {
    var str_before_username = "<div class='my-3 p-3 bg-white rounded box-shadow'><h6 class='border-bottom border-gray pb-2 mb-0'>Recent updates</h6><div class='media text-muted pt-3'>";
    var str_before_username2 = "<p class='media-body pb-3 mb-0 small lh-125 border-bottom border-gray'><strong class='d-block text-gray-dark'>";
    if (photo == '') {
        var profile_photo = "<img src='img/test.svg' alt='' class='mr-2 rounded' style='height:32px; width:32px;'></img>";
    } else {
        var profile_photo = "<img src=" + photo + " alt='' class='mr-2 rounded' style='height:32px; width:32px;'></img>";
    }
    var buttons = "<div class=\"btn-group-sm\" role=\"group\" aria-label=\"Basic example\"><button type=\"button\" class=\"btn btn-light opt-btn\" id=\"\">Replay</button><button type=\"button\" class=\"btn btn-light opt-btn\" id=\"\">Delete</button></div>";
    var str_after_content = "</p></div>" + buttons + "</div>\n";
    return str_before_username + profile_photo + str_before_username2 + email + "</strong>" + post + str_after_content;
}

window.onload = function () {
    init();
};