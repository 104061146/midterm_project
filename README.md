# Software Studio 2019 Spring Midterm Project

## Topic
* Project Name : NTHU Forum
* Key functions
    1. User page
    2. Post list page
    3. Post page
    4. Leave comment under a page
    
* Other functions
    1. remove post
    2. sort the post

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：[https://midterm-project-df3d7.firebaseapp.com]

# Components Description : 
1. Membership Mechanism: By clicking sign up button, you can sign up an account with full name, email and password. Then, you can use email and password to signin the website.
![](./demo/signin.png)
2. Firebase Page:  
We deploy the app with firebase.
3. Database:  
We save the post and user information on the realtime database.
3. RWD:  
We collapse the sidebar when the width of window is smaller than a threshold. In profile, we use bootstrap `grid` layout to enhance the RWD. The user interface looks good even on the mobile device.   
![](demo/rwd1.png)
![](demo/rwd2.png)
4. User page:  
By clicking `profile`, you can see the user page. In user pagem you can see and edit the personal infomation and the posts you posted. For personal information, we use a `navs` to seperate the personal information into three part: overview, basic information and experience.
5. Post list page:  
There two post list page: **academic** and **entertainment** (see the sidebar). In the post list page, you can see all of the posts which only contain title, author email and partial post content. If you want to see the whole post, you can click `read more...`.
6. Post page:  
By clicking `read more...` of a post, a modal will show up and display the whole post.
7. Leave comment under a page:  
In buttom of post `modal`, you can leave the comment under the post. The comment will be saved in firebase database.
8. Third-Party Sign In:  
We support Google and Facebook sign in. The setting for third-parity sign in is almost as the same as the lecture slides except for the privacy policy URL. Privacy policy URL is a link to inform the user to know what information the App. will receive and use. To get a privacy policy URL, we use [free privacy policy URL generator](https://www.termsfeed.com/privacy-policy-generator/).
9. Notification:  
To send the notification to user, we need to follow the three steps. First, we need to check whether the browser support the notification.  
    ```javascript
    if (!("Notification" in window)){...}
    ```   
    Second, we need to get the permission from the user.  
    ```javascript
    Notification.requestPermission().then(...)
    ```
    Finally, we can send notification to user.
    ```javascript
    var notification = new Notification("NTHU Forum", {
                    body: "New Post Available!"
    });
    ```
    Our notification works well on both Safari, Chrome, Firefox and mobile device.  
    <table>
		<tr>
			<td>
				<img src=demo/note.png>
			</td>
			<td rowspan=3 >
				<img src=demo/note1.png>
			</td>
		</tr>
		<tr>
			<td valign=middle>
				<img src=demo/note2.png>
			</td>
		</tr>
		<tr>
			<td valign=middle>
				<img src=demo/note3.png>
			</td>
		</tr>
	</table>

10. Use CSS Animatio:  
    To use the css animation, we need to define a `animation-name` and `animation-duration` first. Then, we can define the animation with `@keyframes`, and the animation is created by gradually changing from one set of css styles to another. In our case, we change `margin-top` from 100% to 1%, which can create a sliding effect.

    ![](./demo/css.gif)
    ```css
    .smb {
        animation-duration: 2s;
        animation-name: slidein;
        margin-bottom: 0;
    }

    @keyframes slidein {
        from {
            margin-top: 100%;
            width: 100%;
        }

        to {
            margin-top: 1%;
            width: 100%;
        }
    }
    ```


# Other Functions Description(1~10%) : 
1. remove post:   
You can delete a post by clicking the X button. This operation will delete the post on firebase as well.  
![](./demo/delete_post.gif)

2. sort the post:
You can sort the post. To change the sort method, you can user the drop-down button `Sort By` to select how to sort the post. There are three sorting options: time, email and title. The default sorting option is sorting by time.
![](./demo/sort.gif)


## Security Report (Optional)
To avoid other corupt the website, one of the effective method is avoid user input HTML. Therefore, we record all the HTML tags and check whether a string contain the HTML tag. The code is shown below. `checkHTML` is the function to determine whether a string is HTML code, and all the input to the website will pass to `checkHTML`. 

```javascript
const tags = [
    "a", "abbr", "address", "area", "article", "aside", "audio", "b", "base",
    "bdi", "bdo", "blockquote", "body", "br", "button", "canvas", "caption", "cite",
    "code", "col", "colgroup", "data", "datalist", "dd", "del", "details", "dfn", "dialog",
    "div", "dl", "dt", "em", "embed", "fieldset", "figcaption", "figure", "footer",
    "form", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hgroup", "hr",
    "html", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "legend", "li",
    "link", "main", "map", "mark", "math", "menu", "menuitem", "meta", "meter", "nav",
    "noscript",
    "object", "ol", "optgroup", "option", "output", "p", "param", "picture", "pre",
    "progress", "q", "rb", "rp", "rt", "rtc", "ruby", "s", "samp", "script",
    "section", "select", "slot", "small", "source", "span", "strong", "style", "sub",
    "summary", "sup", "svg", "table", "tbody", "td", "template", "textarea", "tfoot",
    "th", "thead", "time", "title", "tr", "track", "u", "ul", "var", "video", "wbr"
];
const full = new RegExp(tags.map(x => `<${x}\\b[^>]*>`).join('|'), 'i');
checkHTML = input => full.test(input);
```